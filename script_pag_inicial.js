const url = "lobinhos.json"
var y = 0
let main_div = document.querySelector(".main_div")

function criar_card(imagem,nome,desc,idade,x){
    console.log(x)  
    var r = parseInt(Math.random() * 1000)
    console.log(r)
    if(y == 0 && x == r){
        
        let main_wolf_div = document.createElement("div")
        let wolf_container_div = document.createElement("div")
        let img_wolf_div = document.createElement("div")
        let wolf_img = document.createElement("img")
        let wolf_stats_div = document.createElement("div")
        let wolf_name = document.createElement("p")
        let wolf_age = document.createElement("p")
        let wolf_desc = document.createElement("p")

        main_wolf_div.classList.add("main_div")
        wolf_container_div.classList.add("wolf_container")
        img_wolf_div.classList.add("wolf_img_div")
        img_wolf_div.id = "wolf_img1"
        wolf_img.classList.add("wolf_img")
        wolf_stats_div.classList.add("wolf_stats")
        wolf_name.classList.add("name")
        wolf_age.classList.add("age")
        wolf_desc.classList.add("desc")

        wolf_desc.innerText = desc
        wolf_age.innerText = idade
        wolf_name.innerText = nome
        wolf_img.setAttribute("src",imagem)

        img_wolf_div.append(wolf_img)
        wolf_container_div.append(img_wolf_div)
        wolf_stats_div.append(wolf_name)
        wolf_stats_div.append(wolf_age)
        wolf_stats_div.append(wolf_desc)
        wolf_container_div.append(wolf_stats_div)
        main_wolf_div.append(wolf_container_div)
        main_div.append(main_wolf_div)
        y += 1

    }else{
        if(y ==1 && x == r){
        
            let main_wolf_div = document.createElement("div")
            let wolf_container_div = document.createElement("div")
            let img_wolf_div = document.createElement("div")
            let wolf_img = document.createElement("img")
            let wolf_stats_div = document.createElement("div")
            let wolf_name = document.createElement("p")
            let wolf_age = document.createElement("p")
            let wolf_desc = document.createElement("p")

            main_wolf_div.classList.add("main_div")
            wolf_container_div.classList.add("wolf_container") //aqui
            img_wolf_div.classList.add("wolf_img_div")
            img_wolf_div.id = "wolf_img2"
            wolf_img.classList.add("wolf_img")
            wolf_stats_div.classList.add("wolf_stats")
            wolf_name.classList.add("name")
            wolf_age.classList.add("age")
            wolf_desc.classList.add("desc")

            wolf_desc.innerText = desc
            wolf_age.innerText = idade
            wolf_name.innerText = nome
            wolf_img.setAttribute("src",imagem)

            
            wolf_stats_div.append(wolf_name)
            wolf_stats_div.append(wolf_age)
            wolf_stats_div.append(wolf_desc)
            wolf_container_div.append(wolf_stats_div)
            img_wolf_div.append(wolf_img)
            wolf_container_div.append(img_wolf_div)
            main_wolf_div.append(wolf_container_div)
            main_div.append(main_wolf_div)
            y += 1
        }
 
    }    
    
    if(x == 999 && y == 1){
        main_div.innerHTML = ""
        y = 0
        get_msg()
    }
    if(x == 999 && y == 0){
        main_div.innerHTML = ""
        y = 0
        get_msg()
    }

}




function get_msg(){

    const fetchconfig = {
        "method":"GET"
    }

    fetch(url,fetchconfig)

    .then((resposta) => {
        resposta.json()
        
        .then((resposta) =>{
            console.log(resposta)
            resposta.forEach((elemento,indice) => {
                criar_card(elemento.imagem,elemento.nome,elemento.descricao,elemento.idade,indice)
                
            });
            
        })
        .catch((erro) => {
            console.log(erro)
        })
    })

    .catch((erro) =>{
        console.log(erro)
    })
}

get_msg()
y = 0